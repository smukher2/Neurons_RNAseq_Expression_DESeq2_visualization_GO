# Please help by citing this article and methods as follows:  
### Mukherjee S. Immune gene network of neurological diseases: Multiple sclerosis (MS), Alzheimer's disease (AD), Parkinson's disease (PD) and Huntington's disease (HD). Heliyon. 2021 Dec 1;7(12):e08518. doi: 10.1016/j.heliyon.2021.e08518. PMID: 34926857; PMCID: PMC8649734.  

### Mukherjee S. Quiescent stem cell marker genes in glioma gene networks are sufficient to distinguish between normal and glioblastoma (GBM) samples. Sci Rep. 2020 Jul 2;10(1):10937. doi: 10.1038/s41598-020-67753-5. PMID: 32616845; PMCID: PMC7363816.  

###  Mukherjee S, Klaus C, Pricop-Jeckstadt M, Miller JA, Struebing FL. A Microglial Signature Directing Human Aging and Neurodegeneration-Related Gene Networks. Front Neurosci. 2019 Jan 24;13:2. doi: 10.3389/fnins.2019.00002. PMID: 30733664; PMCID: PMC6353788.  


Format: the pipelines are provided as 1)Rmarkdown files knit to html 2) Rmarkdown files knit to pdf and 3) Rmarkdown or rmd files that can be opened and run on Rstudio by just hitting the "run current chunk" button

This pipeline shows steps from input count data-(to)->Differential gene expression (DGE)-(to)->Visualization-(to)->GO analysis
1) Differential gene expression analysis with edgeR/DESeq2
2) Visualization with boxplots, PCA, heat-map and corelation matrix
3) GO analysis
This pipeline can be applied to any RNA-seq count data.

Sample dataset is obtained from: Cembrowski MS, Wang L, Sugino K, Shields BC et al. Hipposeq: a comprehensive RNA-seq database of gene expression in hippocampal principal neurons. Elife 2016 Apr 26;5:e14997. PMID: 27113915
Disclaimer: I have not contrinuted to the above publication. 

The input data example here consists of RNA-seq done on neurons obtained from different regions of hippocampus, namely the CA1, CA2, CA3, CA4 and DG. Furthermore, the regional dorsal and ventral sampels were seperately collected. 
The following modificatinos were made: 1In this analysis DG, CA1, CA3 were used, while CA2 and CA4 has not been used, since they do not contain sub-region data. 


The total RNA was isolated from each sample using PicoPure RNA Isolation kit and reads were mappted using Tophat2 on mm9 mouse genome. The data was mapped on mm9 genome. 

Details on input data files:
1) GSE74985 from NCBI/GEO and is labeled as "GSE74985_mergedCount.txt"

On commandline (Terminal on Mac/Linux, Cygwin terminal on Windows) the data was downloaded as follows

#installs wget in linux ubuntu

sudo apt-get install wget 

#ftp download from NCBI GEO usin wget 

wget ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE74nnn/GSE74985/suppl/GSE74985_mergedCount.txt.gz

#unzip the downloaded file

gunzip GSE74985_mergedCount.txt.gz

2) A metadata file was created matching the columns of the input count file GSE74985_mergedCount.txt and is labeled as "GSE74985metadata.txt". The metadata consists of information from hippocampus region. 2The following modification was made:
An additional individual metadata has beeen added to mimic real life situations where same animal and patient contribute to different tissue or cellular samples. 

